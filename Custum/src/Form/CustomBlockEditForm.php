<?php

namespace Drupal\custom_block_icons\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Custom block edit form.
 */
class CustomBlockEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_block_icons_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['custom_menu'] = [
      '#type' => 'custom_block_icons_icons',
    ];

    // Other form elements for the custom block edit form.
    // ...
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Process the submitted form data.
    // ...
  }

}
