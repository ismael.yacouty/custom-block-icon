**Custom Block Icon Module**
**Description:**
The Custom Block Icon module is a Drupal module that allows you to easily customize the icons displayed for your custom blocks. With this module, you can choose from a wide range of icons or upload your own custom icons to add visual flair to your Drupal website.

**Installation**:
Download the Custom Block Icon module from the Drupal.org website or using Composer.
Extract the module files into the modules directory of your Drupal installation.
Enable the module by navigating to the Extend page (/admin/modules) in your Drupal site administration.
Search for "Custom Block Icon" in the module list and enable it by checking the checkbox next to it.
Click on the "Install" button at the bottom of the page to install the module and its dependencies.

**Configuration**:
After enabling the Custom Block Icon module, go to the Block layout configuration page (/admin/structure/block) in your Drupal site administration.
Edit the custom block for which you want to set a custom icon.
In the block configuration form, you will find a new field labeled "Custom Icon." Click on the field to open the icon selection dialog.
Choose an icon from the available options or upload your own custom icon.
Save the block configuration to apply the changes.
Usage
Once you have configured the Custom Block Icon module, the selected or uploaded icon will be displayed alongside the custom block on your Drupal website. The icon will provide a visual representation of the block's content or purpose.

You can repeat the configuration process for each custom block that you want to assign a specific icon to. You can also update the icons anytime by editing the block configuration.

**Compatibility**:
The Custom Block Icon module is compatible with Drupal 8 , Drupal 9 and Drupal 10. It relies on Drupal's block system and extends its functionality by allowing the customization of block icons.

Currently WIP
